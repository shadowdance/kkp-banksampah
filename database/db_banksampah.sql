-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2018 at 11:27 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_banksampah`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_customer`
--

CREATE TABLE `tb_customer` (
  `id_customer` int(10) NOT NULL,
  `nm_customer` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `RT` char(2) NOT NULL,
  `RW` char(2) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `email` varchar(15) NOT NULL,
  `status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_customer`
--

INSERT INTO `tb_customer` (`id_customer`, `nm_customer`, `alamat`, `RT`, `RW`, `no_telp`, `email`, `status`) VALUES
(2018020701, 'PT Sampah ', 'Petukangan', '07', '05', '08123456789', 'sampah@gmail.co', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_detilpenjualan`
--

CREATE TABLE `tb_detilpenjualan` (
  `no_transaksi` char(16) NOT NULL,
  `id_sampah` tinyint(2) NOT NULL,
  `jumlah` int(2) NOT NULL DEFAULT '0',
  `hargajual` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_detilpenjualan`
--

INSERT INTO `tb_detilpenjualan` (`no_transaksi`, `id_sampah`, `jumlah`, `hargajual`, `total`) VALUES
('JS070220180001', 1, 1, '7000.00', '7000.00'),
('JS070220180002', 3, 1, '5000.00', '5000.00'),
('JS070220180003', 5, 5, '5000.00', '25000.00'),
('JS070220180004', 5, 2, '8000.00', '16000.00'),
('JS070220180005', 5, 5, '6000.00', '30000.00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_detiltabungan`
--

CREATE TABLE `tb_detiltabungan` (
  `no_transaksi` char(16) NOT NULL,
  `id_nasabah` int(10) NOT NULL,
  `id_sampah` tinyint(2) NOT NULL,
  `jumlah` int(2) NOT NULL,
  `total` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_detiltabungan`
--

INSERT INTO `tb_detiltabungan` (`no_transaksi`, `id_nasabah`, `id_sampah`, `jumlah`, `total`) VALUES
('TB070220180001', 2018060201, 1, 5, '25000.00'),
('TB070220180002', 2018060201, 3, 1, '4000.00'),
('TB070220180003', 2018060202, 2, 2, '13000.00'),
('TB070220180004', 2018060203, 3, 3, '12000.00'),
('TB070220180005', 2018070204, 5, 5, '18500.00'),
('TB070220180006', 2018070204, 5, 5, '18500.00'),
('TB070220180007', 2018060203, 6, 5, '20000.00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kas`
--

CREATE TABLE `tb_kas` (
  `no_transaksi` char(16) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `nominal` decimal(10,0) NOT NULL DEFAULT '0',
  `ket_transaksi` varchar(10) NOT NULL,
  `status` enum('masuk','keluar') NOT NULL,
  `dest` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kas`
--

INSERT INTO `tb_kas` (`no_transaksi`, `tgl_transaksi`, `nominal`, `ket_transaksi`, `status`, `dest`) VALUES
('TK280220180001', '2018-02-09', '40000', 'KET001', 'masuk', 'customer'),
('TK280220180002', '2018-02-15', '10000', 'KET002', 'keluar', 'pln');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ketkas`
--

CREATE TABLE `tb_ketkas` (
  `id_ket` varchar(10) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `status` enum('masuk','keluar') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ketkas`
--

INSERT INTO `tb_ketkas` (`id_ket`, `keterangan`, `status`) VALUES
('KET001', 'penjualan tunai', 'masuk'),
('KET002', 'pembayaran listrik', 'keluar');

-- --------------------------------------------------------

--
-- Table structure for table `tb_nasabah`
--

CREATE TABLE `tb_nasabah` (
  `id_nasabah` int(10) NOT NULL,
  `no_ktp` char(16) NOT NULL,
  `nm_nasabah` varchar(30) DEFAULT NULL,
  `jenkel` enum('1','0') NOT NULL,
  `alamat` text,
  `RT` char(2) NOT NULL,
  `RW` char(2) NOT NULL,
  `no_telp` varchar(15) DEFAULT NULL,
  `email` varchar(15) DEFAULT NULL,
  `tabungan` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` enum('1','0') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_nasabah`
--

INSERT INTO `tb_nasabah` (`id_nasabah`, `no_ktp`, `nm_nasabah`, `jenkel`, `alamat`, `RT`, `RW`, `no_telp`, `email`, `tabungan`, `status`) VALUES
(2018060201, '0123456789102018', 'Budi', '1', 'Ciledug Raya', '05', '03', '021456789', 'Budi@gmail.com', '11000.00', '1'),
(2018060202, '0123456789102019', 'Luhur', '0', 'Ciledug', '04', '02', '021123789', 'Luhur@gmail.com', '13000.00', '1'),
(2018060203, '0123456789102020', 'Cakti', '1', 'Petukangan', '03', '01', '021365498', 'Cakti@gmail.com', '32000.00', '1'),
(2018070204, '0123456789102021', 'Faiz', '1', 'Bintaro', '06', '04', '085872088105', 'faizalvian97@gm', '30000.00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pencairantab`
--

CREATE TABLE `tb_pencairantab` (
  `no_transaksi` char(16) NOT NULL DEFAULT '',
  `tgl_transaksi` date NOT NULL DEFAULT '0000-00-00',
  `id_nasabah` int(10) NOT NULL DEFAULT '0',
  `jumlah_tarik` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pencairantab`
--

INSERT INTO `tb_pencairantab` (`no_transaksi`, `tgl_transaksi`, `id_nasabah`, `jumlah_tarik`) VALUES
('PT070220180001', '2018-02-07', 2018060201, '9000.00'),
('PT070220180002', '2018-02-07', 2018070204, '7000.00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_penjualan`
--

CREATE TABLE `tb_penjualan` (
  `tgl_transaksi` date NOT NULL DEFAULT '0000-00-00',
  `no_transaksi` char(16) NOT NULL,
  `id_petugas` char(10) NOT NULL,
  `id_customer` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_penjualan`
--

INSERT INTO `tb_penjualan` (`tgl_transaksi`, `no_transaksi`, `id_petugas`, `id_customer`) VALUES
('2018-02-07', 'JS070220180001', 'admin', 2018020701),
('2018-02-07', 'JS070220180002', 'admin', 2018020701),
('2018-02-07', 'JS070220180003', 'admin', 2018020701),
('2018-02-07', 'JS070220180004', 'admin', 2018020701),
('2018-02-07', 'JS070220180005', 'admin', 2018020701);

-- --------------------------------------------------------

--
-- Table structure for table `tb_petugas`
--

CREATE TABLE `tb_petugas` (
  `id_petugas` char(10) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nm_petugas` varchar(30) DEFAULT NULL,
  `alamat` text,
  `no_telp` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_petugas`
--

INSERT INTO `tb_petugas` (`id_petugas`, `password`, `nm_petugas`, `alamat`, `no_telp`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'Zill', 'jl.larangan', '08213124');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sampah`
--

CREATE TABLE `tb_sampah` (
  `id_sampah` tinyint(2) NOT NULL,
  `jenis_sampah` varchar(20) DEFAULT NULL,
  `stok` int(5) NOT NULL DEFAULT '0',
  `hargabeli` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_sampah`
--

INSERT INTO `tb_sampah` (`id_sampah`, `jenis_sampah`, `stok`, `hargabeli`) VALUES
(1, 'Alumunium', 6, '5000.00'),
(2, 'Besi', 9, '6500.00'),
(3, 'Kaleng', 10, '4000.00'),
(4, 'Koran', 3, '3800.00'),
(5, 'Kardus', 10, '3700.00'),
(6, 'Emberan/Plastik', 0, '4000.00'),
(7, 'Gelas Mineral', 0, '4000.00'),
(8, 'Botol Mineral', 0, '4200.00'),
(9, 'Botol Kaca Pet A', 0, '4300.00'),
(10, 'Buku', 0, '7000.00'),
(11, 'Tutup Botol', 0, '3000.00'),
(12, 'SP/Boncos', 0, '3500.00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tabungan`
--

CREATE TABLE `tb_tabungan` (
  `tgl_transaksi` date NOT NULL DEFAULT '0000-00-00',
  `no_transaksi` char(16) NOT NULL,
  `id_nasabah` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tabungan`
--

INSERT INTO `tb_tabungan` (`tgl_transaksi`, `no_transaksi`, `id_nasabah`) VALUES
('2018-02-07', 'TB070220180001', 2018060201),
('2018-02-07', 'TB070220180002', 2018060201),
('2018-02-07', 'TB070220180003', 2018060202),
('2018-02-07', 'TB070220180004', 2018060203),
('2018-02-07', 'TB070220180005', 2018070204),
('2018-02-07', 'TB070220180006', 2018070204),
('2018-02-07', 'TB070220180007', 2018060203);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_customer`
--
ALTER TABLE `tb_customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `tb_detilpenjualan`
--
ALTER TABLE `tb_detilpenjualan`
  ADD KEY `no_transaksi` (`no_transaksi`),
  ADD KEY `id_sampah` (`id_sampah`);

--
-- Indexes for table `tb_detiltabungan`
--
ALTER TABLE `tb_detiltabungan`
  ADD KEY `no_transaksi` (`no_transaksi`),
  ADD KEY `id_sampah` (`id_sampah`),
  ADD KEY `id_nasabah` (`id_nasabah`);

--
-- Indexes for table `tb_kas`
--
ALTER TABLE `tb_kas`
  ADD KEY `ket_transaksi` (`ket_transaksi`);

--
-- Indexes for table `tb_ketkas`
--
ALTER TABLE `tb_ketkas`
  ADD PRIMARY KEY (`id_ket`);

--
-- Indexes for table `tb_nasabah`
--
ALTER TABLE `tb_nasabah`
  ADD PRIMARY KEY (`id_nasabah`);

--
-- Indexes for table `tb_pencairantab`
--
ALTER TABLE `tb_pencairantab`
  ADD PRIMARY KEY (`no_transaksi`),
  ADD KEY `id_nasabah` (`id_nasabah`);

--
-- Indexes for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  ADD PRIMARY KEY (`no_transaksi`),
  ADD KEY `id_petugas` (`id_petugas`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Indexes for table `tb_petugas`
--
ALTER TABLE `tb_petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `tb_sampah`
--
ALTER TABLE `tb_sampah`
  ADD PRIMARY KEY (`id_sampah`);

--
-- Indexes for table `tb_tabungan`
--
ALTER TABLE `tb_tabungan`
  ADD PRIMARY KEY (`no_transaksi`),
  ADD KEY `no_nasabah` (`id_nasabah`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_detilpenjualan`
--
ALTER TABLE `tb_detilpenjualan`
  ADD CONSTRAINT `tb_detilpenjualan_ibfk_4` FOREIGN KEY (`id_sampah`) REFERENCES `tb_sampah` (`id_sampah`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_detiltabungan`
--
ALTER TABLE `tb_detiltabungan`
  ADD CONSTRAINT `tb_detiltabungan_ibfk_3` FOREIGN KEY (`id_sampah`) REFERENCES `tb_sampah` (`id_sampah`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tb_detiltabungan_ibfk_4` FOREIGN KEY (`id_nasabah`) REFERENCES `tb_nasabah` (`id_nasabah`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_kas`
--
ALTER TABLE `tb_kas`
  ADD CONSTRAINT `tb_kas_ibfk_1` FOREIGN KEY (`ket_transaksi`) REFERENCES `tb_ketkas` (`id_ket`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_pencairantab`
--
ALTER TABLE `tb_pencairantab`
  ADD CONSTRAINT `tb_pencairantab_ibfk_1` FOREIGN KEY (`id_nasabah`) REFERENCES `tb_nasabah` (`id_nasabah`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  ADD CONSTRAINT `tb_penjualan_ibfk_3` FOREIGN KEY (`id_customer`) REFERENCES `tb_customer` (`id_customer`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_tabungan`
--
ALTER TABLE `tb_tabungan`
  ADD CONSTRAINT `tb_tabungan_ibfk_2` FOREIGN KEY (`id_nasabah`) REFERENCES `tb_nasabah` (`id_nasabah`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
